Accounts.onCreateUser(function(options, user) {
  user.profile = options.profile || {};
  var username  = options.profile.name;
  var uniqeUsername = options.profile.name;
  while(true){

    var isExist = Meteor.users.find({username : uniqeUsername}).count() == 1;

    if(!isExist){
      break;
    }

    uniqeUsername = username + (Math.floor(Math.random() * 1000) + 1);
  }

  user.username = uniqeUsername;
  if(options.profileImagePath)
  user.profileImagePath = options.profile.profileImagePath;
  if(options.status)
  user.status = options.profile.status;
  return user;
},( error ) => {
      if ( error ) {
        console.log( error.reason, 'danger' );
      }
});
