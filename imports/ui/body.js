import './pages/accounts/account.js';
import './pages/accounts/account.html';

import './pages/Home/home.js';
import './pages/Home/home.html';

import './components/login/login.js';
import './components/login/login.html';

import './components/signup/signup.js';
import './components/signup/signup.html';

import './components/logout/logout.js';
import './components/logout/logout.html';

import './components/chat/chat.js';
import './components/chat/chat.html';

import './components/list/list.js';
import './components/list/list.html';

import '../api/accounts.config.js';
import './layouts/mainLayout/mainLayout.html';

import './pages/notFound/notFound.html';
import './pages/settings/settings.html';

import './components/settingList/settingsList.html'
import './components/settingList/settingsList.js'

import './components/signup/upload.html';
