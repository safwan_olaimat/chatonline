import { Template } from 'meteor/templating';

import './login.html'
Template.login.events({
'submit form': function(event){
    event.preventDefault();
    var email = $("#email").val();
    var pwd = $("#pwdl").val();
    Meteor.loginWithPassword(email, pwd, function(err) {
      if (err) {
        alert(err.reason);
      }
    });
}
});
