import { Template } from 'meteor/templating';
import { Images } from '../../../api/upload.js';
import { ReactiveVar } from 'meteor/reactive-var'
import './signup.html';
import './upload.html';
var currentUpload="";
Template.uploadForm.onCreated(function () {
  currentUpload = new ReactiveVar(false);
});
Template.uploadForm.helpers({
  currentUpload() {
    return currentUpload.get();
  }
});
Template.register.events({
    'submit form': function(event) {
      event.preventDefault();
      var fname = $("#fname").val();
      var lname = $("#lname").val();
      var name = fname + " " + lname;
      var email = $("#signup-email").val();
      var pwd = $("#signup-pwd").val();
      Accounts.createUser({
           email: email,
           password: pwd,
           profile: {
             name: name,
             profileImagePath:'nothing'
           }
      }, function(err){
        alert(err.reason);
      });
    }

  });
