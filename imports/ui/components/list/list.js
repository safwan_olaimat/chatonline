import { Template } from 'meteor/templating';
import { Friends } from '../../../api/users.js';
import './list.html';



Template.list.helpers({
  allusers:function(){
    return  Meteor.users.find({_id:{$ne:Meteor.userId()}});
  },
});
Template.list.events({
  'click #req':function(event){
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
  },'click #logout':function(event){
      Meteor.logout();
  },
  'click #setting':function(event){
    window.location.assign("/settings/"+Meteor.userId());
  }
  ,
  'click .close':function(event){
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
  },
  'click .useritem':function(event){
    var selectedId=$(event.target).data("id");
    console.log(selectedId);
    window.location.assign("/"+selectedId);
  }
});
