import {
  Template
} from 'meteor/templating';
import {
  messages
} from '../../../api/messages.js';
import './chat.html';
Template.registerHelper('eq', (a, b) => {
  return a === b;
});
Template.chat.onRendered(function() {
  $(".messages").scrollTop(10000000000);

   $("#texxt").keypress(function(e) {
     if (e.keyCode === 13) {
       $(".send").click();
       return false;
     }
   });
});
Template.chat.helpers({
  msToDate:function(time){
    var date = new Date(time);
    return date.toUTCString();
  },
  Messages: function() {
    return messages.find({});
  },
  info: function() {
    return {
      friendId: Router.current().params.id,
      friendname: Meteor.users.findOne({
        _id: Router.current().params.id
      }).profile.name,
      myname: Meteor.users.findOne({
        _id: Meteor.userId()
      }).profile.name
    };
  }
});
Template.chat.events({
  'click .send': function(event) {
    var obj = {
      sender: "" + Meteor.userId(),
      reciver: "" + Router.current().params.id,
      text: $("#texxt").val(),
      createdAt: new Date().getTime()
    };
    Meteor.call('insert', obj, function(err, res) {
      if (err)
        console.log(err.reason);
      else {
        $("#texxt").val('');
      }
    });
    Meteor.setTimeout(function(){
      $(".messages").scrollTop(10000000000);
    },100);
  }
})
