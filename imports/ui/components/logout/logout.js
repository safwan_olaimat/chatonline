import { Template } from 'meteor/templating';
import './logout.html';

Template.logout.events({
  'click #logout': function(event) {
   Meteor.logout();
  }

});
