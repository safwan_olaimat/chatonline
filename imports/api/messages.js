import {
  Mongo
} from 'meteor/mongo';

export const messages = new Mongo.Collection("messages");
const messages_schema = new SimpleSchema({
  sender: {
    type: String,
    label: "sender"
  },
  reciver: {
    type: String,
    label: "reciver"
  },
  text: {
    type: String,
    label: "text"
  },
  createdAt: {
    type: new Date(),
    label: "createdAt"
  }
});
messages.attachSchema(messages_schema);

if (Meteor.isServer) {

  Meteor.publish('mymsgs', function mymsgs(FriendId) {
    return messages.find({
      $or: [{sender: Meteor.userId(),reciver: FriendId},{reciver: Meteor.userId(),sender: FriendId}]
    });
  });
  Meteor.methods({
    'insert': function(obj) {
      var count1 = Meteor.users.find({
        _id: obj.sender
      }).count();
      var count2 = Meteor.users.find({
        _id: obj.reciver
      }).count();
      if (count1 == 0 || count2 == 0)
        throw new Meteor.Error(400, "Missing sender or reciver Id");
      messages.insert({
        sender: "" + obj.sender,
        reciver: "" + obj.reciver,
        text: obj.text,
        createdAt: obj.createdAt
      });
    },
  });
}
