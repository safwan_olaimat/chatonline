import { Mongo } from 'meteor/mongo';

export const Friends = new Mongo.Collection('Friends');

const relation_schema =   new SimpleSchema({
  userId1:{
  type: String,
  label:"userId1"
  },
  userId2:{
  type: String,
  label:"userId2"
  }
});
Friends.attachSchema(relation_schema);

if(Meteor.isServer)
{
    Meteor.publish('allusers',function allusers(){
    return Meteor.users.find({},{fields:{username:1,createdAt:1 ,profile:1}});
  });
  Meteor.methods({
      Follow:function(userid2){
        var rel={userId1:Meteor.userId(),userId2:userid2};
        Friends.update(rel,{$set:rel},{ upsert: true });
      }
  });
}
