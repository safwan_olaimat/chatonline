Router.configure({
  notFoundTemplate:'p404'
})
Router.route('/:id', {
  layoutTemplate: 'mainLayout',
  waitOn: [
    function() {

      return [Meteor.subscribe('allusers'),Meteor.subscribe('mymsgs', this.params.id)];
    }
  ],
  action: function() {
    if(Meteor.userId())
      this.render('home', {to: 'MainContainer'});
    else {
      this.render('accounts', {to: 'MainContainer'});
    }
  },
  data: function() {
    var userid = this.params.id;
    return {
      friendId: userid,

    }
  }
});
Router.route('/', {
  layoutTemplate: 'mainLayout',
  waitOn: [
    function() {

      return Meteor.subscribe('allusers');
    }
  ],
  action: function() {
    if(Meteor.userId())
    this.render('home', {to: 'MainContainer'});
    else {
      this.render('accounts', {to: 'MainContainer'});
    }
  },
  data: function() {

  }
});
Router.route('/settings/:id',{
   layoutTemplate: 'mainLayout',
   waitOn: [
     function() {
     }
   ],
   action: function() {
     if(Meteor.userId()==this.params.id)
    {
      this.render('settingsPage', {to: 'MainContainer'});
    }else{
      this.render('p404', {to: 'MainContainer'});
    }
   },
   data: function() {
     return {id:this.params.id};
   }
})

//export MONGO_URL=mongodb://user:password@dsNNNNNN.mongolab.com:port/db
//mongodb://<skolaimat>:<xx11xx11>@ds051980.mlab.com:51980/chaton
